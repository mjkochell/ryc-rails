require 'aws-sdk-sns'

class SubmissionsController < ApplicationController
  def create
    p = submission_params
    @sub = Submission.create!(p)
    if ENV['POST_TO_SNS'] == 'true'
        post_to_sns(@sub)
    end

    render json: {success: true}
  end

  def index
    token = params[:token]
    if token != ENV['ADMIN_TOKEN']
      render html: '', status: 404
      return
    end

    render html: render_subs_html(Submission.all).html_safe
  end

  private

  def post_to_sns(sub)
    sns = Aws::SNS::Resource.new(region: ENV['SNS_REGION'])
    topic = sns.topic(ENV['SNS_TOPIC_ARN'])
    subject = "#{sub.email} has submitted your form"
    topic.publish message: render_notification_email(sub).html_safe, subject: subject
  end

  def render_notification_email(sub)
    <<~EOF
        #{sub.email} submitted the form.
        #{JSON.pretty_generate sub.as_json}
    EOF
  end

  def render_subs_html(subs)
    <<~EOF
        <pre>
            #{JSON.pretty_generate subs.as_json}
        </pre>
    EOF
  end

  def submission_params
    params.require(:submission).permit(
      :charity_id,
      :charity_name,
      :charity_text,
      :charity_location,
      :charity_location_text,
      :charity_group,
      :merchant_id,
      :merchant_name,
      :merchant_text,
      :merchant_location,
      :merchant_location_text,
      :name,
      :email,
      :phone,
      :receipt_url,
    )
  end
end
