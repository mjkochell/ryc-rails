class FormsController < ActionController::Base
  def submissions
    @submission = Submission.new
    render 'forms/submissions/new'
  end

  def qrcode
    render 'forms/qrcode/new'
  end

  private
end
