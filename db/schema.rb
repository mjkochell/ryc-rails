# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2020_04_05_030012) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "pgcrypto"
  enable_extension "plpgsql"

  create_table "submissions", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.string "merchant_id"
    t.string "merchant_name"
    t.string "merchant_text"
    t.string "merchant_location"
    t.string "merchant_location_text"
    t.string "charity_name"
    t.string "charity_text"
    t.string "charity_id"
    t.string "charity_location"
    t.string "charity_location_text"
    t.string "charity_group"
    t.string "email"
    t.string "phone"
    t.string "name"
    t.string "receipt_url"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "deleted_at"
    t.index ["charity_group"], name: "index_submissions_on_charity_group"
    t.index ["charity_id"], name: "index_submissions_on_charity_id"
    t.index ["charity_location"], name: "index_submissions_on_charity_location"
    t.index ["charity_name"], name: "index_submissions_on_charity_name"
    t.index ["deleted_at"], name: "index_submissions_on_deleted_at"
    t.index ["email"], name: "index_submissions_on_email"
    t.index ["merchant_id"], name: "index_submissions_on_merchant_id"
    t.index ["merchant_location"], name: "index_submissions_on_merchant_location"
    t.index ["merchant_name"], name: "index_submissions_on_merchant_name"
    t.index ["phone"], name: "index_submissions_on_phone"
  end

end
