class CreateSubmissions < ActiveRecord::Migration[5.2]
  def change
    create_table :submissions, id: :uuid do |t|
      t.string :merchant_id, index: true
      t.string :merchant_name, index: true
      t.string :merchant_text
      t.string :merchant_location, index: true
      t.string :merchant_location_text
      t.string :charity_name, index: true
      t.string :charity_text
      t.string :charity_id, index: true
      t.string :charity_location, index: true
      t.string :charity_location_text
      t.string :charity_group, index: true
      t.string :email, index: true
      t.string :phone, index: true
      t.string :name
      t.string :receipt_url

      t.timestamps
      t.datetime :deleted_at, index: true
    end
  end
end
