Rails.application.routes.draw do
  resources :ping, only: [:index]
  resources :submissions
  # resources :audio_files, only: [:index, :show, :new, :create, :update] do
  #   member do
  #     post :trim
  #   end
  # end

  resources :users

  resources :forms do
    collection do
      get :submissions
      get :qrcode
    end
  end
end
